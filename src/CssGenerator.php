<?php

namespace Drupal\published_state_indicator;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Service description.
 */
class CssGenerator {

  const CSS_FILE_DIR = 'public://published_state_indicator/';
  const CSS_FILE_PATH = self::CSS_FILE_DIR . 'labels.css';

  /**
   * The file handler.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Constructs a CssGenerator object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   */
  public function __construct(FileSystemInterface $file_system, ConfigManagerInterface $config_manager) {
    $this->fileSystem = $file_system;
    $this->configManager = $config_manager;
  }

  /**
   * Method description.
   */
  public function generateCssFile() {
    $config = $this->configManager->getConfigFactory()->getEditable('published_state_indicator.settings');
    $font_size = $config->get('font_size');
    $colour_config = $config->get('workflows');

    $css = "span[class^=\"psi-\"] {
      font-size: $font_size !important;
    }\n\r";

    // Construct the labels CSS for each workflow + state.
    foreach ($colour_config as $workflow_id => $states) {
      foreach ($states as $state_id => $colours) {
        extract($colours);
        $workflow_id = str_replace('_', '-', $workflow_id);
        $state_id = str_replace('_', '-', $state_id);
        $css .= "span[class=\"psi--$workflow_id--$state_id\"] {
          color: $color_primary;
          border-color: $color_primary;
          background-color: $color_secondary;
        }\n\r";
      }
    }

    // Write the CSS to a file in the public filesystem which will be added
    // to the existing module library using hook_library_info_alter().
    $directory = self::CSS_FILE_DIR;
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    if ($this->fileSystem->saveData($css, self::CSS_FILE_PATH, FileSystemInterface::EXISTS_REPLACE)) {
      \Drupal::service('file_system')->chmod(self::CSS_FILE_PATH);
    }
  }

}
